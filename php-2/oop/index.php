<?php 

require_once 'Animal.php';
require_once 'Ape.php';
require_once 'Frog.php';

echo '<br><br>=========== RELEASE 0 ==============<br>';
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo '<br>'. $sheep->legs; // 2
echo '<br>'. $sheep->cold_blooded; // false

echo '<br>'.'<br>'.$sheep->get_name();
echo '<br>'.$sheep->get_legs();
echo '<br>'.$sheep->get_cold_blooded();



echo '<br><br>=========== RELEASE 1 ==============<br>';
// echo '<br><br>=========================<br>';
$sheep = new Animal("shaun", 4);
echo 'Name : '. $sheep->name; // "shaun"
echo '<br>legs : '. $sheep->legs; // 4
echo '<br>cold blooded : '.$sheep->cold_blooded; // false

echo '<br><br>';

$kodok = new Frog("buduk");
echo 'Name : '. $kodok->name; // "shaun"
echo '<br>legs : '. $kodok->legs; // 2
echo '<br>cold blooded : '.$kodok->cold_blooded; // false
echo '<br>Jump : '.$kodok->jump() ; // "hop hop"


echo '<br><br>';
$sungokong = new Ape("kera sakti");
echo 'Name : '. $sungokong->name; // "shaun"
echo '<br>legs : '. $sungokong->legs; // 2
echo '<br>cold blooded : '.$sungokong->cold_blooded; // false
echo '<br>Yell : '.$sungokong->yell(); // "Auooo"

?>